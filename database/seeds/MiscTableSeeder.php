<?php

use Illuminate\Database\Seeder;
use App\MiscellaneousSeed;

class MiscTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('misc')->delete();
        $json = File::get("database/data/misc.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
        	MiscellaneousSeed::create(array(
        		'sublevel_id'=> $obj->sublevel_id,
   				'quote'=> $obj->quote,
   				'description'=> $obj->description,
   				'icon'=> $obj->icon
   			
        	));
        }
    }
}
