<?php

use Illuminate\Database\Seeder;
use App\ProductSeed;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        $json = File::get("database/data/products_alter.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
        	ProductSeed::create(array(
        	'quantity'=> $obj->quantity,
   				'price'=> $obj->price,
   				'available'=> $obj->available,
   				'sublevel_id'=> $obj->sublevel_id,
   				'name'=> $obj->name,
   				'serial'=> $obj->serial,
          'icon' => $obj->icon,
          'id_number' => $obj->id_number
        	));
        }
    }
}
