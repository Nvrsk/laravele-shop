<?php $__env->startSection('content'); ?>

    <div class="container-fluid product-lore">
         


        <div class="row">
            <div class="col-md-4 product-sidebar">
              <?php if($product->available == 0): ?>
            <div class="alert alert-danger text-center mt-3" data-icon="&#xe023;">
                <?php echo e(session()->flash('error', 'Out of stock... You ate them all!! ')); ?>

                <?php echo e(session()->get('error')); ?>

            </div>
              <?php endif; ?>
              <section class="m-5">
                <?php $__currentLoopData = $misc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $misc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="container text-center">
                    <div class="foodicon foodicon--<?php echo e($misc->icon); ?>" style="line-height: 1;"></div>
                  <h4 style="text-transform: capitalize; font-family: 'Pacifico', cursive;"> <?php echo e($product->name); ?></h4>
                  
                  <p class="lead text-muted"><?php echo e($misc->quote); ?></p>
                  
                 
                </div>
          
            
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >

              <div class="foodicon foodicon--<?php echo e($product->icon); ?> text-center mt-5"></div>
              <div class="card-body">
               
                <p class="card-text mb-2"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline"><h3 class="d-inline">$ <?php echo e($product->price); ?></h3></span></p>
                <div >
                  <form action="<?php echo e(url('/cart')); ?>" method="POST" class="side by side">
                    <input type="hidden" name="id" value="<?php echo e($product->id_number); ?>">
                    <input type="hidden" name="name" value="<?php echo e($product->name); ?>">
                    <input type="hidden" name="available" value="<?php echo e($product->available); ?>">
                    <input type="hidden" name="price" value="<?php echo e($product->price); ?>">
                    <input type="hidden" name="icon" value="<?php echo e($product->icon); ?>">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                  </form>

                  
                </div>
               
              </div>
            </div>
          
             </section>
                
            </div>

            <div class="col-md-8 product-lore">
                <div class="navbar navbar-dark">
                <div class="container d-flex justify-content-end">
                 
                        <a href="<?php echo e(url('/shop')); ?>" class="navbar-brand btn-sm shop-button" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Back to Shop<span class="pl-3" data-icon="&#xe02a;"></span></a>
        
   
                 
           
                </div>
              </div>
                    
    <div class="container mt-3">
                        <h6 class="mt-5 mb-3 text-muted">Description:</h6>
                      
                        <p><?php echo e($misc->description); ?></p>
                        <hr class="mt-4">
                        <h6 class="mt-1 mb-3 text-muted">Nutritional Values:</h6>
                        <table class="table table-sm table-responsive-sm" style="border-bottom-left-radius: 2.5px; border-bottom-right-radius: 2.5px;background: #2a2b30;">

  <tbody>
    <tr >
      <th scope="row">Calories</th>
      <td>200</td>
      <th>Calories from Fat</th>
      <td>8</td>
    </tr>
    <tr>
      <th scope="row">Fat</th>
      <td>1g</td>
      <th>Saturated Fat</th>
      <th>Trans Fat</th>
    </tr>
     <tr>
      <th scope="row"></th>
      <td></td>
      <td>0g</td>
      <td>0g</td>
    </tr>
      <tr>
      <th scope="row">Carbohydrate</th>
      <td>36g</td>
      <th>Protein</th>
      <td>13g</td>
    </tr>
  </tbody>
</table>


                    
</div>
<hr class="mt-4">
       
<h6 class="text-muted mb-2">You may taste these...</h6>
<div class="row aditional-products mt-5">
            <?php $__currentLoopData = $interested; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-3 col-xs-2">
                    <div class="thumbnail">
                        <div class="caption text-center">
                            <div class="foodicon foodicon--<?php echo e($product->icon); ?>" style="line-height: 1;"></div>
                            <a href="<?php echo e(url('/product', [$product->name])); ?>"><h6 style="text-transform: capitalize; font-family: 'Pacifico', cursive;"><?php echo e($product->name); ?></h6>
                            <p><strong>$</strong><?php echo e($product->price); ?></p>
                            </a>
                        </div> 

                    </div> 
                </div> 
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div> 
 
</div> 

 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div> 

   

  
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>