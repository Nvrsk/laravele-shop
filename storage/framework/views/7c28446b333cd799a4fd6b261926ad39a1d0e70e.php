<?php $__env->startSection('content'); ?>

    <div class="container">
         <div class="navbar navbar-dark">
                <div class="container d-flex justify-content-end">
                 
                        <a href="<?php echo e(url('/shop')); ?>" class="navbar-brand btn-sm shop-button" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Back to Shop<span class="pl-3" data-icon="&#xe02a;"></span></a>
        
   
                 
           
                </div>
              </div>
                    
   <div class="row mx-5">
            <h3 class="d-inline" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Your Vault </h3><div class=" d-inline foodicon foodicon--basket" style="line-height: .5;color: #cecece;"></div>
   </div>

        <?php if(session()->has('success_message')): ?>
            <div class="alert alert-success text-center mt-2    ">
                <?php echo e(session()->get('success_message')); ?>

            </div>
        <?php endif; ?>

        <?php if(session()->has('error_message')): ?>
            <div class="alert alert-danger text-center">
                <?php echo e(session()->get('error_message')); ?>

            </div>
        <?php endif; ?>
<div class="row">
    <div class="col-md-8">
    

        <?php if(sizeof(Cart::instance('wishlist')->content()) > 0): ?>

             <table class="table table-sm table-responsive-sm my-5" style="background-color: #1c1d22;">
                <thead>
                    <tr>
                    
                        <th id="cart" class="text-center">Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                      
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>
                    
                <tbody>
                    <?php $__currentLoopData = Cart::instance('wishlist')->content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                         <td class="text-center"><a href="<?php echo e(url('/product', [$item->id])); ?>" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color: #9b59b6;" ><?php echo e($item->name); ?></a></td>

                        <td>$<?php echo e($item->subtotal); ?></td>
                         <td>
                            <form action="<?php echo e(url('wishlist', [$item->rowId])); ?>" method="POST" class="side-by-side">
                                <?php echo csrf_field(); ?>

                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                            </form>

                            <form action="<?php echo e(url('switchToCart', [$item->rowId])); ?>" method="POST" class="side-by-side">
                                <?php echo csrf_field(); ?>

                                <input type="submit" class="btn btn-success btn-sm" value="To Cart">
                            </form>
                        </td>
                    </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
            </table>
</div>
         

      
<div class="col-md-4 product-lore my-5" style="border-radius: 5px;">
    <div class="row my-5 justify-content-center">
    
        <div class="btn-group-vertical">
        <a href="<?php echo e(url('/shop')); ?>"><button  type="submit" class=" btn-block " data-icon="&#xe02a;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Continue eati...I mean shooping</span></button></a>

         <a href="<?php echo e(url('/checkout')); ?>"><button  type="submit" class=" btn-block " data-icon="&#xe02a;" style="font-size: .7em;color: #cecece; background: #8e44ad;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;">Pay for what you've done</span></button></a>
           
        <a href="<?php echo e(url('/vault')); ?>"> <button type="submit" class=" btn-block" data-icon="&#xe00d;" style="font-size: .7em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the vault!</span></button></a>


            
        <form action="<?php echo e(url('/emptyVault')); ?>" method="POST">
                    <?php echo csrf_field(); ?>

                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class=" btn-block" data-icon="&#xe00d;" style="font-size: .7em;color: #cecece;background-color: #e74c3c"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >Empty Vault</span></button>
        </form>
            


        </div>
       
        </div>         
</div>
</div>

        <?php else: ?>

            <h5 class="text-center my-5" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">You have no items in your vault</h5>
            <a href="<?php echo e(url('/shop')); ?>" class="btn-block" data-icon="&#xe009;" style="font-size: 1em;color: #8e44ad"> Go back to find some...</a>

        <?php endif; ?>

  

    </div> 

<?php $__env->stopSection(); ?>



          
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>