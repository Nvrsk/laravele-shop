<?php $__env->startSection('content'); ?>

    <div class="container">
               <div class="navbar navbar-dark">
                <div class="container d-flex justify-content-end">
                 
                        <a href="<?php echo e(url('/shop')); ?>" class="navbar-brand btn-sm shop-button" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Back to Shop<span class="pl-3" data-icon="&#xe02a;"></span></a>
        
   
                 
           
                </div>
              </div>
                    
   <div class="row mx-5">
            <h3 class="d-inline" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Your Basket </h3><div class=" d-inline foodicon foodicon--basket" style="line-height: .5;color: #cecece;"></div>
   </div>

        <?php if(session()->has('success_message')): ?>
            <div class="alert alert-success text-center mt-2    ">
                <?php echo e(session()->get('success_message')); ?>

            </div>
        <?php endif; ?>

        <?php if(session()->has('error_message')): ?>
            <div class="alert alert-danger text-center">
                <?php echo e(session()->get('error_message')); ?>

            </div>
        <?php endif; ?>

  <div class="row">
      <div class="col-md-8">
         
        <?php if(sizeof(Cart::content()) > 0): ?>

            <table class="table table-sm table-responsive-sm my-5" style="background-color: #1c1d22;">
                <thead>
                    <tr>
                    
                        <th id="cart" class="text-center">Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                      
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>
                    
                <tbody>
                    <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td class="text-center"><a href="<?php echo e(url('/product', [$item->id])); ?>" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color: #9b59b6;" ><?php echo e($item->name); ?></a></td>
                        <td>
                            <select class="quantity" data-id="<?php echo e($item->rowId); ?>">
                                <option <?php echo e($item->qty == 10 ? 'selected' : ''); ?>>10</option>
                                <option <?php echo e($item->qty == 50 ? 'selected' : ''); ?>>50</option>
                                <option <?php echo e($item->qty == 100 ? 'selected' : ''); ?>>100</option>
                                <option <?php echo e($item->qty == 500 ? 'selected' : ''); ?>>500</option>
                            
                            </select>
                        </td>
                        <td>$<?php echo e($item->subtotal); ?></td>
                       
                        <td>
                            <form action="<?php echo e(url('/globalcart', [$item->rowId])); ?>" method="POST">
                                <?php echo csrf_field(); ?>

                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class=" btn-block " data-icon="&#xe006;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;">Remove...Why?</span></button>
                            </form>
                        </td>
                    </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>$<?php echo e(Cart::instance('default')->subtotal()); ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Tax</td>
                        <td>$<?php echo e(Cart::instance('default')->tax()); ?></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">$<?php echo e(Cart::total()); ?></td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>

     
        <?php else: ?>

            <h5 class="text-center my-5" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">You have no items in your shopping cart</h5>
            <a href="<?php echo e(url('/shop')); ?>" class="btn-block" data-icon="&#xe020;" style="font-size: 1em;color: #8e44ad"> Go back to spend some...</a>


        <?php endif; ?> 
      </div>
      <div class="col-md-4 product-lore my-5" style="border-radius: 5px;">
<div class="row my-5 justify-content-center">
    
        <div class="btn-group-vertical">
        <a href="<?php echo e(url('/shop')); ?>"><button  type="submit" class=" btn-block " data-icon="&#xe02a;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Continue eati...I mean shooping</span></button></a>

         <a href="<?php echo e(url('/checkout')); ?>"><button  type="submit" class=" btn-block " data-icon="&#xe02c;" style="font-size: .7em;color: #cecece; background: #8e44ad;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Pay for what you've done</span></button></a>


            
        <form action="<?php echo e(url('/emptyCart')); ?>" method="POST">
                    <?php echo csrf_field(); ?>

                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class=" btn-block" data-icon="&#xe006;" style="font-size: .7em;color: #cecece;background-color: #e74c3c"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >Empty Basket</span></button>
        </form>
            


</div>
       
</div>         
            

          
           

      </div>
  </div>



      

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('extra-js'); ?>
    <script>
        (function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '<?php echo e(url("/globalcart")); ?>' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '<?php echo e(url('/globalcart')); ?>';
                  }
                });

            });

        })();

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>