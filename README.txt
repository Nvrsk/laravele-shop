-La e-shop se creo bajo modelos con Facades y Dependency Injection.

-No se implementaron relaciones belongsTo HasMany por practicidad. 

-Fue necesario crear un key de tipo incremental para evitar duplicados en el carro. 

-La primera coleccion de elementos muestra un grupo de lo que hay disponible, no se extendio a las demas colecciones por redundancia. (implementable)

-La validacion de la existencia se realiza a traves de la vista y al vez desde el controlador filtrando el query por false/true e 

imprimiendolo en las tarjetas de los productos una alerta, cualquier implementacion seria valida.

-El GlobalShop muestra solo aquello en existencia, y una coleccion final muestra lo que esta fuera de existencia. La compra lleva directo al carro.

-vista independiente por productos de Shop a traves de wildcards.

-Incremento de cantidades, remover y enviar elementos a lista de espera funcionales.

Instalacion:

Crear la base de datos: -Version de la DB:

                        mysql 15.1 Distrib 10.1.26-MariaDB

                        create table test;

-composer update

(Contiene modelos de autoseeding desde el archivo .json)

-Ejecutar:
 
php artisan db:seed

-php artisan serve

-localhost:8000
