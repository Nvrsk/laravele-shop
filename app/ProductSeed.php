<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSeed extends Model
{
   public $timestamps = false;

   protected $table = 'products';

   protected $fillable = [

   	'quantity',
   	'price',
   	'available',
   	'sublevel_id',
   	'name',
   	'serial',
      'icon',
      'id_number'

   ];

}
