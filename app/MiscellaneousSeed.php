<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiscellaneousSeed extends Model
{
       public $timestamps = false;

   protected $table = 'misc';

   protected $fillable = [

   	'sublevel_id',
   	'quote',
   	'description',
   	'icon'
   	

   ];
}
