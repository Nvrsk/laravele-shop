<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalShop;

class GlobalShopController extends Controller
{
    public function index()  {



	    $drinks_unsort = GlobalShop::drinksglobal()->where('available',1)->get();

        $drinks = $drinks_unsort->sortBy('price')->values()->all();


        $bakery_unsort = GlobalShop::breakfastglobal()->where('available',1)->get();

        $bakery = $bakery_unsort->sortBy('price')->values()->all();


        $restaurant_unsort = GlobalShop::lunchglobal()->where('available',1)->get();

        $restaurant = $restaurant_unsort->sortBy('price')->values()->all();


        $vineyard_unsort = GlobalShop::winesglobal()->where('available',1)->get();

        $vineyard = $vineyard_unsort->sortBy('price')->values()->all();


        $outstock_unsort = GlobalShop::where('available',0)->get(); 

        $outstock = $outstock_unsort->sortBy('price')->values()->all();

        



        return view('pages.globalshop',compact('drinks','bakery','restaurant','vineyard','outstock'));







    }



    
}
