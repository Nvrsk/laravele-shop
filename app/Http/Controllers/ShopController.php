<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Product; use App\Misc;

class ShopController extends Controller
{

    public function index()
    {
       
        $sugary_unsort = Product::drinks()->where('sublevel_id',2)->get();

        $sugary = $sugary_unsort->sortBy('price')->values()->all();

        $sugarless_unsort = Product::drinks()->where('sublevel_id',3)->get();

        $sugarless = $sugarless_unsort->sortBy('price')->values()->all();

        $jam_unsort = Product::breakfast()->where('sublevel_id',5)->get();

        $jam = $jam_unsort->sortBy('price')->values()->all();

        $cheese_unsort = Product::breakfast()->where('sublevel_id',6)->get();

        $cheese = $cheese_unsort->sortBy('price')->values()->all();

        $wheat_unsort = Product::breakfast()->where('sublevel_id',7)->get();

        $wheat = $wheat_unsort->sortBy('price')->values()->all();

        $noodless_unsort = Product::lunch()->where('sublevel_id',8)->get();

        $noodless = $noodless_unsort->sortBy('price')->values()->all();

        $salad_unsort = Product::lunch()->where('sublevel_id',9)->get();

        $salad = $salad_unsort->sortBy('price')->values()->all();

        $fruit_salad_unsort = Product::lunch()->where('sublevel_id',10)->get();

        $fruit_salad = $fruit_salad_unsort->sortBy('price')->values()->all();

        $red_unsort = Product::wines()->where('sublevel_id',12)->get();

        $red = $red_unsort->sortBy('price')->values()->all();

        $white_unsort = Product::wines()->where('sublevel_id',13)->get();

        $white = $white_unsort->sortBy('price')->values()->all();

        $instock_unsort = Product::breakfast()->where('available',1)->get();

        $instock = $instock_unsort->sortBy('price')->values()->all();



        return view('pages.shop',compact('sugary','sugarless','jam','cheese','wheat','noodless','salad','fruit_salad','red', 'white','instock'));







    }


    public function show($id)
    {

        $misc = Misc::all()->random(1);

        $product = Product::where('id_number', $id)->firstOrFail();
  
        $interested = Product::where('id_number', '!=', $id)->get()->random(4);

      

        return view('pages.product')->with(['product' => $product, 'interested' => $interested, 'misc' => $misc]);


    }
}


