@extends('layout')
@section('content')
<div class="container">
   <div class="navbar navbar-dark">
      <div class="container d-flex justify-content-end">
         <a href="{{ url('/shop') }}" class="navbar-brand btn-sm shop-button" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Back to Shop<span class="pl-3" data-icon="&#xe02a;"></span></a>
      </div>
   </div>
   <div class="container text-center">
      <div class="foodicon foodicon--smoothie" style="line-height: 1;"></div>
      <h4 style="text-transform: capitalize; font-family: 'Pacifico', cursive;">el baratón foodstore</h4>
   </div>
   <nav class="nav justify-content-center mb-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">
      <a class="nav-link" href="{{ url('/cart') }}" data-icon="&#xe015;"> basket</a>
      <a class="nav-link" href="{{ url('/globalshop') }}" data-icon="&#xe02e;"> Warehouse</a>
   </nav>
   <div class="row mx-5 p-3 product-lore" style="border-radius: 5px;">
      <h3 class="d-inline" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Your Pantry </h3>
      <div class=" d-inline foodicon foodicon--basket" style="line-height: .5;color: #cecece;"></div>
   </div>
   @if (session()->has('success_message'))
   <div class="alert alert-success text-center mt-2    ">
      {{ session()->get('success_message') }}
   </div>
   @endif
   @if (session()->has('error_message'))
   <div class="alert alert-danger text-center">
      {{ session()->get('error_message') }}
   </div>
   @endif
   <div class="row">
      <div class="col-md-8">
         @if (sizeof(Cart::instance('wishlist')->content()) > 0)
         <table class="table table-sm table-responsive-sm my-5" style="background-color: #1c1d22;">
            <thead>
               <tr>
                  <th id="cart" class="text-center">Product</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th class="column-spacer"></th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               @foreach (Cart::instance('wishlist')->content() as $item)
               <tr>
                  <td class="text-center"><a href="{{ url('/product', [$item->id]) }}" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color: #9b59b6;" >{{ $item->name }}</a></td>
                  <td>${{ $item->subtotal }}</td>
                  <td>
                     <form action="{{ url('wishlist', [$item->rowId]) }}" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class=" btn-block " data-icon="&#xe006;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;">Remove? Whatever...</span></button>
                     </form>
                     <form action="{{ url('switchToCart', [$item->rowId]) }}" method="POST" class="side-by-side">
                        {!! csrf_field() !!}
                        <button type="submit" class=" btn-block" data-icon="&#xe020;" style="font-size: .7em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" > I'll get it now!</span></button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
      <div class="col-md-4 product-lore my-5" style="border-radius: 5px;">
         <div class="row my-5 justify-content-center">
            <div class="btn-group-vertical">
               <a href="{{ url('/shop') }}"><button  type="submit" class=" btn-block " data-icon="&#xe02a;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Continue eati...I mean shooping</span></button></a>
               <a href="{{ url('/cart') }}"> <button type="submit" class=" btn-block" data-icon="&#xe020;" style="font-size: .7em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" > To the basket!</span></button></a>
               <form action="{{ url('/emptyVault') }}" method="POST">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class=" btn-block" data-icon="&#xe006;" style="font-size: .7em;color: #cecece;background-color: #e74c3c"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >Empty Vault</span></button>
               </form>
            </div>
         </div>
      </div>
   </div>
   @else
   <h5 class="text-center my-5" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">You have no items in your pantry</h5>
   <a href="{{ url('/shop') }}" class="nav-link" data-icon="&#xe009;" style="font-size: 1em;color: #8e44ad; text-decoration: none;"> Go back to find some...</a>
   @endif
</div>
@endsection