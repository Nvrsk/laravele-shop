@extends ('layout')

@section ('content')

<div id="st-container" class="st-container">


    
    <div class=" st-menu st-effect-2 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

      <h2 class="icon icon-video" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Our Stock</h2>

      <a class="nav-link active" id="v-pills-breakfast-tab" data-toggle="pill" href="#v-pills-breakfast" role="tab" aria-controls="v-pills-breakfast" aria-selected="false"><div class="d-inline foodicon foodicon--bread pr-1" style="font-size: 1.2em;color: #fff"></div>Breakfast</a>

      <a class="nav-link" id="v-pills-drinks-tab" data-toggle="pill" href="#v-pills-drinks" role="tab" aria-controls="v-pills-drinks" aria-selected="true"><div class="d-inline pr-3" data-icon="&#xe016;" ></div>Drinks</a>

      <a class="nav-link" id="v-pills-lunch-tab" data-toggle="pill" href="#v-pills-lunch" role="tab" aria-controls="v-pills-lunch" aria-selected="false"><div class="d-inline pr-3" data-icon="&#xe026;"></div>Lunch </a>

      <a class="nav-link" id="v-pills-wines-tab" data-toggle="pill" href="#v-pills-wines" role="tab" aria-controls="v-pills-wines" aria-selected="false"><div class="d-inline foodicon foodicon--grape pr-1" style="font-size: 1.2em;color: #fff"></div>Wines</a>

    </div>

  
    <div class="st-pusher">

      <div class="st-content">
        <div class="st-content-inner">
          <div class="main clearfix">
        
            
              
              <div class="collapse" id="navbarHeader" style="background-color: #1c1d22; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-8 py-4">
                      <h4  data-icon="&#xe008;" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;"> Warehouse</h4>
                      <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa fuga officia, sint omnis corporis adipisci reprehenderit.</p>
                    </div>
                    <div class="col-sm-4 py-4">
                      <h4  style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">What we do</h4>
                      <ul class="list-unstyled">
                        <li><a href="#" class="text-white" data-icon="&#xe02e;"> Delivery wherever you need</a></li>
                        <li><a href="#" class="text-white" data-icon="&#xe02f;"> Overseas sales</a></li>
                        <li><a href="#" class="text-white" data-icon="&#xe00c;"> Packing and branding</a></li>

                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="navbar navbar-dark">
                <div class="container d-flex justify-content-between ">
                    <div class="st-trigger-effects">
                        <button class="navbar-brand trigger btn-sm shop-button" data-effect="st-effect-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Shop<span class="pl-3" data-icon="&#xe02a;"></span></button>
        
   
                    </div>
                 
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                </div>
              </div>
                 <nav class="nav justify-content-center mb-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">
                      <a class="nav-link" href="{{ url('/cart') }}" data-icon="&#xe015;"> basket</a>
                      <a class="nav-link" href="{{ url('/wishlist') }}" data-icon="&#xe00d;"> pantry</a>
                      <a class="nav-link" href="{{ url('/globalshop') }}" data-icon="&#xe02e;"> Warehouse</a>
                  </nav>
          
              <section >

                <div class="container text-center">
                    <div class="foodicon foodicon--smoothie" style="line-height: 1;"></div>
                  <h4 style="text-transform: capitalize; font-family: 'Pacifico', cursive;">el baratón foodstore</h4>
                  
                  <p class="lead text-muted">Check what's now on the table</p>
         
                   
                 
                </div>
              </section>
          
<div class="tab-content" id="v-pills-tabContent">
  <div class="tab-pane fade show active" id="v-pills-breakfast" role="tabpanel" aria-labelledby="v-pills-breakfast-tab">
    <div  class="container" style="height: 100%">
        <div class="d-flex justify-content-center text-center mb-4 mt-0">
            <div class="row w-100 align-items-middle">
                   <div class="col-md-12">
                    <div class="btn-group d-none d-sm-flex justify-content-center">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button " data-filter="all"> All you can eat </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "jam"> Croissant</button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "cheese"> Egg & Cheese sandwich </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "wheat"> Speedy Oatmeal </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "instock"> See what's left... </button>
                    </div>
                     <div class="d-block d-sm-none">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block  btn-primary filter-button" data-filter="all"> All you can eat </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block  btn-primary filter-button" data-filter = "jam"> Croissant</button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block  btn-primary filter-button" data-filter = "cheese"> Egg & Cheese sandwich </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block  btn-primary filter-button" data-filter = "wheat"> Speedy Oatmeal </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block  btn-primary filter-button" data-filter = "instock"> See what's left... </button>
                    </div>
          </div> 
                   </div>
        </div>

      
        <div class="row">
           @foreach ($cheese as $product)
          <div class="col-md-4 filter  cheese" data-cat="cheese">
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}}  text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>
                @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
          @endforeach
          @foreach ($jam as $product)
          <div class="col-md-4 filter jam" data-cat="jam">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach
          @foreach ($wheat as $product)
          <div class="col-md-4 filter wheat" data-cat="wheat">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach
                @foreach ($instock as $product)
          <div class="col-md-4 filter instock" data-cat="instock">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach
        </div>
              
    </div>  
  </div>
  <div class="tab-pane fade" id="v-pills-drinks" role="tabpanel" aria-labelledby="v-pills-drinks-tab">
    <div  class="container" style="height: 100%">

        <div class="d-flex justify-content-center text-center mb-4 mt-0">
            <div class="row w-100 align-items-middle">
                   <div class="col-md-12">
                    <div class="btn-group d-none d-sm-flex justify-content-center">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter="all"> Why not both? </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "carton"> Pasteurized </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "blender"> Natural </button>
                    </div>

                    <div class="d-block d-sm-none">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter="all"> Why not both? </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "carton"> Pasteurized </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "blender"> Natural </button>
                    </div>
          </div> 
                   </div>
        </div>

     
        <div class="row">
           @foreach ($sugary as $product)
          <div class="col-md-4 filter blender" data-cat="blender">
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
          @endforeach
          @foreach ($sugarless as $product)
          <div class="col-md-4 filter carton" data-cat="carton">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach
        </div>
           
    </div>  

   </div>
  <div class="tab-pane fade" id="v-pills-lunch" role="tabpanel" aria-labelledby="v-pills-lunch-tab">
    <div  class="container" style="height: 100%">

        <div class="d-flex justify-content-center text-center mb-4 mt-0">
            <div class="row w-100 align-items-middle">
                   <div class="col-md-12">
                    <div class="btn-group d-none d-sm-flex justify-content-center">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter="all"> Happy Lunch! </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "noodless"> Noodles </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "salad"> Salad </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "fruits"> Fruits Salad </button>
                    </div>
                    <div class="d-block d-sm-none">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter="all"> Happy Lunch! </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "noodless"> Noodles </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "salad"> Salad </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "fruits"> Fruits Salad </button>
                    </div>
          </div> 
                   </div>
        </div>

     
        <div class="row">
           @foreach ($salad as $product)
          <div class="col-md-4 filter salad" data-cat="salad">
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
            <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
          @endforeach
          @foreach ($noodless as $product)
          <div class="col-md-4 filter noodless" data-cat="noodles">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
               <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach
          @foreach ($fruit_salad as $product)
          <div class="col-md-4 filter fruits" data-cat="fruits">
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
           
    </div>  
  </div>
  <div class="tab-pane fade" id="v-pills-wines" role="tabpanel" aria-labelledby="v-pills-wines-tab">
        <div  class="container" style="height: 100%">
        <div class="d-flex justify-content-center text-center mb-4 mt-0">
            <div class="row w-100 align-items-middle">
                   <div class="col-md-12">
                    <div class="btn-group d-none d-sm-flex justify-content-center">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter="all"> Maybe some water? </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "red"> Red Whines</button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn btn-primary filter-button" data-filter = "white"> The White ones</button>
                    </div>
                    <div class="d-block d-sm-none">
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter="all"> Maybe some water? </button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "red"> Red Whines</button>
                        <button style="font-size: .7em; font-weight: lighter;" type="button" class="btn-block btn-primary filter-button" data-filter = "white"> The White ones</button>
                    </div>
          </div> 
                   </div>
        </div>

      
        <div class="row">
           @foreach ($white as $product)
          <div class="col-md-4 filter  white" data-cat="white">
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
          @endforeach
               @foreach ($red as $product)
          <div class="col-md-4 filter red" data-cat="red">     
            <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
              <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
              <div class="card-body">
                <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>      @if ($product->available == 0)
                <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('offstock_notice', 'Sorry... No more left.') }}
                {{ session()->get('offstock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>
              
                @else

                 <p class="card-text"><strong style="color:#f3efe0;" value="{{ $product->available }}">  {{  session()->flash('instock_notice', 'You can have it!') }}
                {{ session()->get('instock_notice') }}</strong><span id="stock" class=" ml-2 d-inline"></span><span></span></p>

                @endif
                   <a href="{{ url('/product', [$product->id_number]) }}"> <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button></a>
              </div>
            </div>
          </div>
           @endforeach

        </div>
              
    </div> 
  </div>
</div>

    </div>
    </div>
    </div>
    </div>
  </div>
@endsection