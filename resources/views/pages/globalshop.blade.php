@extends ('layout')
@section ('content')
<div id="st-container" class="st-container">
   <div class=" st-menu st-effect-2 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <h2 class="icon icon-video" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Our Warehouse</h2>
      <a class="nav-link active" id="v-pills-breakfast-tab" data-toggle="pill" href="#v-pills-breakfast" role="tab" aria-controls="v-pills-breakfast" aria-selected="false">
         <div class="d-inline foodicon foodicon--bread pr-1" style="font-size: 1.2em;color: #fff"></div>
         Bakery
      </a>
      <a class="nav-link" id="v-pills-drinks-tab" data-toggle="pill" href="#v-pills-drinks" role="tab" aria-controls="v-pills-drinks" aria-selected="true">
         <div class="d-inline pr-3" data-icon="&#xe016;" ></div>
         Drinks
      </a>
      <a class="nav-link" id="v-pills-lunch-tab" data-toggle="pill" href="#v-pills-lunch" role="tab" aria-controls="v-pills-lunch" aria-selected="false">
         <div class="d-inline pr-3" data-icon="&#xe026;"></div>
         Restaurant
      </a>
      <a class="nav-link" id="v-pills-wines-tab" data-toggle="pill" href="#v-pills-wines" role="tab" aria-controls="v-pills-wines" aria-selected="false">
         <div class="d-inline foodicon foodicon--grape pr-1" style="font-size: 1.2em;color: #fff"></div>
         Vineyard
      </a>
      <a class="nav-link" id="v-pills-outofstock-tab" data-toggle="pill" href="#v-pills-outofstock" role="tab" aria-controls="v-pills-outofstock" aria-selected="false">
         <div class="d-inline pr-3" data-icon="&#xe01e;"></div>
         Out of shelves
      </a>
   </div>
   <div class="st-pusher">
      <div class="st-content">
         <div class="st-content-inner">
            <div class="main clearfix">
               <div class="collapse" id="navbarHeader" style="background-color: #1c1d22; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
                  <div class="container">
                     <div class="row">
                        <div class="col-sm-8 py-4">
                           <h4  data-icon="&#xe008;" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;"> Warehouse</h4>
                           <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa fuga officia, sint omnis corporis adipisci reprehenderit.</p>
                        </div>
                        <div class="col-sm-4 py-4">
                           <h4  style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">What we do</h4>
                           <ul class="list-unstyled">
                              <li><a href="#" class="text-white" data-icon="&#xe02e;"> Delivery wherever you need</a></li>
                              <li><a href="#" class="text-white" data-icon="&#xe02f;"> Overseas sales</a></li>
                              <li><a href="#" class="text-white" data-icon="&#xe00c;"> Packing and branding</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="navbar navbar-dark">
                  <div class="container d-flex justify-content-between ">
                     <div class="st-trigger-effects">
                        <button class="navbar-brand trigger btn-sm shop-button" data-effect="st-effect-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Global Shop<span class="pl-3" data-icon="&#xe02f;"></span></button>
                     </div>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                  </div>
               </div>
               <nav class="nav justify-content-center mb-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">
                  <a class="nav-link" href="{{ url('/cart') }}" data-icon="&#xe015;"> basket</a>
                  <a class="nav-link" href="{{ url('/wishlist') }}" data-icon="&#xe00d;"> pantry</a>
                  <a class="nav-link" href="{{ url('/shop') }}" data-icon="&#xe02a;"> Local Shop</a>
               </nav>
               <section >
                  <div class="container text-center">
                     <div class="foodicon foodicon--smoothie" style="line-height: 1;"></div>
                     <h4 style="text-transform: capitalize; font-family: 'Pacifico', cursive;">el baratón foodstore</h4>
                     <p class="lead text-muted">Grab anything you can</p>
                  </div>
               </section>
               <div class="tab-content" id="v-pills-tabContent">
                  <div class="tab-pane fade show active" id="v-pills-breakfast" role="tabpanel" aria-labelledby="v-pills-breakfast-tab">
                     <div  class="container" style="height: 100%">
                        <div class="row">
                           @foreach ($bakery as $product)
                           <div class="col-md-4 filter  cheese" data-cat="cheese">
                              <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
                                 <div class="foodicon foodicon--{{$product->icon}}  text-center mt-5"></div>
                                 <div class="card-body">
                                    <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                                    <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Stock:</strong><span class=" ml-2 d-inline">{{ $product->quantity }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Serial Nº:</strong><span class=" ml-2 d-inline">{{ $product->serial }}</span><span></span></p>
                                    <form action="{{ url('/cart') }}" method="POST" class="side by side">
                                       <input type="hidden" name="id" value="{{ $product->id_number }}">
                                       <input type="hidden" name="name" value="{{ $product->name }}">
                                       <input type="hidden" name="serial" value="{{ $product->serial}}">
                                       <input type="hidden" name="price" value="{{ $product->price }}">
                                       <input type="hidden" name="icon" value="{{$product->icon}}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="v-pills-drinks" role="tabpanel" aria-labelledby="v-pills-drinks-tab">
                     <div  class="container" style="height: 100%">
                        <div class="row">
                           @foreach ($drinks as $product)
                           <div class="col-md-4 filter blender" data-cat="blender">
                              <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
                                 <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
                                 <div class="card-body">
                                    <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                                    <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Stock:</strong><span class=" ml-2 d-inline">{{ $product->quantity }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Serial:</strong><span class=" ml-2 d-inline">{{ $product->serial }}</span><span></span></p>
                                    <form action="{{ url('/cart') }}" method="POST" class="side by side">
                                       <input type="hidden" name="id" value="{{ $product->id_number }}">
                                       <input type="hidden" name="name" value="{{ $product->name }}">
                                       <input type="hidden" name="serial" value="{{ $product->serial}}">
                                       <input type="hidden" name="price" value="{{ $product->price }}">
                                       <input type="hidden" name="icon" value="{{$product->icon}}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="v-pills-lunch" role="tabpanel" aria-labelledby="v-pills-lunch-tab">
                     <div  class="container" style="height: 100%">
                        <div class="row">
                           @foreach ($restaurant as $product)
                           <div class="col-md-4 filter salad" data-cat="salad">
                              <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
                                 <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
                                 <div class="card-body">
                                    <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                                    <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span>$</span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Stock:</strong><span class=" ml-2 d-inline">{{ $product->quantity }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Serial Nº:</strong><span class=" ml-2 d-inline">{{ $product->serial }}</span><span></span></p>
                                    <form action="{{ url('/cart') }}" method="POST" class="side by side">
                                       <input type="hidden" name="id" value="{{ $product->id_number }}">
                                       <input type="hidden" name="name" value="{{ $product->name }}">
                                       <input type="hidden" name="serial" value="{{ $product->serial}}">
                                       <input type="hidden" name="price" value="{{ $product->price }}">
                                       <input type="hidden" name="icon" value="{{$product->icon}}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="v-pills-wines" role="tabpanel" aria-labelledby="v-pills-wines-tab">
                     <div  class="container" style="height: 100%">
                        <div class="row">
                           @foreach ($vineyard as $product)
                           <div class="col-md-4 filter  white" data-cat="white">
                              <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
                                 <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
                                 <div class="card-body">
                                    <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                                    <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Stock:</strong><span class=" ml-2 d-inline">{{ $product->quantity }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Serial Nº:</strong><span class=" ml-2 d-inline">{{ $product->serial }}</span><span></span></p>
                                    <form action="{{ url('/cart') }}" method="POST" class="side by side">
                                       <input type="hidden" name="id" value="{{ $product->id_number }}">
                                       <input type="hidden" name="name" value="{{ $product->name }}">
                                       <input type="hidden" name="serial" value="{{ $product->serial }}">
                                       <input type="hidden" name="price" value="{{ $product->price }}">
                                       <input type="hidden" name="icon" value="{{$product->icon}}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="v-pills-outofstock" role="tabpanel" aria-labelledby="v-pills-outofstock-tab">
                     <div  class="container" style="height: 100%">
                        <div class="row">
                           @foreach ($outstock as $product)
                           <div class="col-md-4 filter  white" data-cat="white">
                              <div class="card mb-5 mx-0 " style=" background-color:#1c1d22!important;" >
                                 <div class="foodicon foodicon--{{$product->icon}} text-center mt-5"></div>
                                 <div class="card-body">
                                    <h4 class="card-title" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">{{$product->name}}</h4>
                                    <p class="card-text"><strong style="color:#f3efe0;">Price:</strong><span class=" ml-2 d-inline">{{ $product->price }}</span><span></span></p>
                                    <p class="card-text"><strong style="color:#f3efe0;">Serial Nº:</strong><span class=" ml-2 d-inline">{{ $product->serial }}</span><span></span></p>
                                    <form action="{{ url('/cart') }}" method="POST" class="side by side">
                                       <input type="hidden" name="id" value="{{ $product->id_number }}">
                                       <input type="hidden" name="name" value="{{ $product->name }}">
                                       <input type="hidden" name="serial" value="{{ $product->serial }}">
                                       <input type="hidden" name="price" value="{{ $product->price }}">
                                       <input type="hidden" name="icon" value="{{$product->icon}}">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <button type="submit" class=" btn-block foodicon foodicon--basket" style="font-size: 1.3em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the basket!</span></button>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection