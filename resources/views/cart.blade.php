@extends('layout')
@section('content')
<div class="container">
   <div class="navbar navbar-dark">
      <div class="container d-flex justify-content-end">
         <a href="{{ url('/shop') }}" class="navbar-brand btn-sm shop-button" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color:#5c5edc;">Back to Shop<span class="pl-3" data-icon="&#xe02a;"></span></a>
      </div>
   </div>
   <div class="container text-center">
      <div class="foodicon foodicon--smoothie" style="line-height: 1;"></div>
      <h4 style="text-transform: capitalize; font-family: 'Pacifico', cursive;">el baratón foodstore</h4>
   </div>
   <nav class="nav justify-content-center mb-2" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">
      <a class="nav-link" href="{{ url('/wishlist') }}" data-icon="&#xe00d;"> pantry</a>
      <a class="nav-link" href="{{ url('/globalshop') }}" data-icon="&#xe02e;"> Warehouse</a>
   </nav>
   <div class="row mx-5 p-3 product-lore" style="border-radius: 5px;">
      <h3 class="d-inline" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">Your Basket </h3>
      <div class=" d-inline foodicon foodicon--basket" style="line-height: .5;color: #cecece;"></div>
   </div>
   @if (session()->has('success_message'))
   <div class="alert alert-success text-center mt-2    ">
      {{ session()->get('success_message') }}
   </div>
   @endif
   @if (session()->has('error_message'))
   <div class="alert alert-danger text-center">
      {{ session()->get('error_message') }}
   </div>
   @endif
   <div class="row">
      <div class="col-md-8">
         @if (sizeof(Cart::content()) > 0)
         <table class="table table-sm table-responsive-sm my-5" style="background-color: #1c1d22;">
            <thead>
               <tr>
                  <th id="cart" class="text-center">Product</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>ID Nº</th>
                  <th class="column-spacer"></th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               @foreach (Cart::content() as $item)
               <tr>
                  <td class="text-center"><a href="{{ url('/product', [$item->id]) }}" style="text-transform: capitalize; font-family: 'Pacifico', cursive; color: #9b59b6;" >{{ $item->name }}</a></td>
                  <td>
                     <select class="quantity" data-id="{{ $item->rowId }}">
                     <option {{ $item->qty == 1   ? 'selected' : '' }}>1    </option>
                     <option {{ $item->qty == 2   ? 'selected' : '' }}>2    </option>
                     <option {{ $item->qty == 3   ? 'selected' : '' }}>3    </option>
                     <option {{ $item->qty == 4   ? 'selected' : '' }}>4    </option>
                     <option {{ $item->qty == 5   ? 'selected' : '' }}>5    </option>    
                     <option {{ $item->qty == 10  ? 'selected' : '' }}>10   </option>
                     <option {{ $item->qty == 50  ? 'selected' : '' }}>50   </option>
                     <option {{ $item->qty == 100 ? 'selected' : ''  }}>100 </option>
                     <option {{ $item->qty == 500 ? 'selected' : ''  }}>500 </option>  
                     </select>
                  </td>
                  <td>${{ $item->subtotal }}</td>
                  <td>{{  $item->id }}</td>
                  <td>
                     <form action="{{ url('cart', [$item->rowId]) }}" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class=" btn-block " data-icon="&#xe006;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"><span class="d-none d-sm-inline">Remove...You sure?</span></span></button>
                     </form>
                     <form action="{{ url('switchToWishlist', [$item->rowId]) }}" method="POST" class="side-by-side">
                        {!! csrf_field() !!}
                        <button type="submit" class=" btn-block" data-icon="&#xe00d;" style="font-size: .7em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" ><span class="d-none d-sm-inline">I'll get it later!</span></span></button>
                     </form>
                  </td>
               </tr>
               @endforeach
               <tr>
                  <td class="table-image"></td>
                  <td></td>
                  <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                  <td>${{ Cart::instance('default')->subtotal() }}</td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td class="table-image"></td>
                  <td></td>
                  <td class="small-caps table-bg" style="text-align: right">Tax</td>
                  <td>${{ Cart::instance('default')->tax() }}</td>
                  <td></td>
                  <td></td>
               </tr>
               <tr class="border-bottom">
                  <td class="table-image"></td>
                  <td style="padding: 40px;"></td>
                  <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                  <td class="table-bg">${{ Cart::total() }}</td>
                  <td class="column-spacer"></td>
                  <td></td>
               </tr>
            </tbody>
         </table>
         @else
         <h5 class="text-center my-5" style="text-transform: capitalize; font-family: 'Pacifico', cursive;">You have no items in your basket</h5>
         <a href="{{ url('/shop') }}" class="btn-block" data-icon="&#xe020;" style="font-size: 1em;color: #8e44ad"> Go back to spend some...</a>
         @endif 
      </div>
      <div class="col-md-4 product-lore my-5" style="border-radius: 5px;">
         <div class="row my-5 justify-content-center">
            <div class="btn-group-vertical">
               <a href="{{ url('/shop') }}"><button  type="submit" class=" btn-block " data-icon="&#xe02a;" style="font-size: .7em;color: #cecece; background: #9b59b6;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Continue eati...I mean shooping</span></button></a>
               <a href="{{ url('/checkout') }}"><button  type="submit" class=" btn-block " data-icon="&#xe02c;" style="font-size: .7em;color: #cecece; background: #8e44ad;"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;"> Pay for what you've done</span></button></a>
               <td class="col-md-2">
                  <form action="/checkout" method="POST">
                     {!! csrf_field() !!}
               <td class="col-md-2">
               <form action="/checkout" method="POST">
               {!! csrf_field() !!}
               </form>
               </td>
               </form>
               </td>
               <a href="{{ url('/wishlist') }}"> <button type="submit" class=" btn-block" data-icon="&#xe00d;" style="font-size: .7em;color: #cecece"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >To the pantry!</span></button></a>
               <form action="{{ url('/emptyCart') }}" method="POST">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class=" btn-block" data-icon="&#xe006;" style="font-size: .7em;color: #cecece;background-color: #e74c3c"> <span style="font-family: 'Lato', sans-serif; text-transform: uppercase; font-size: 1em;" >Empty Basket</span></button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('extra-js')
<script>
   (function(){
   
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
   
       $('.quantity').on('change', function() {
           var id = $(this).attr('data-id')
           $.ajax({
             type: "PATCH",
             url: '{{ url("/cart") }}' + '/' + id,
             data: {
               'quantity': this.value,
             },
             success: function(data) {
               window.location.href = '{{ url('/cart') }}';
             }
           });
   
       });
   
   })();
   
</script>
@endsection